 angular.module('starter.services', [])
     .factory('service', function($http, $q, $window) {
         return {
             get: function(url) {
                 var deferred = $q.defer();
                 $http.get(url)
                     .success(function(data) {
                         deferred.resolve(data);
                     }).error(function(msg, code) {
                         deferred.reject(code);
                     });
                 return deferred.promise;
             },
             post: function(url, obj) {
                 var deferred = $q.defer();
                 $http.post(url, obj)
                     .success(function(data) {
                         deferred.resolve(data);
                     }).error(function(msg, code) {
                         deferred.reject(code);
                     });
                 return deferred.promise;
             }
         };
     })
     .factory('product_list_services', function($http) {
         return {
             getproduct: function() {
                 return $http.get('/json/product_list.json').then(function(data) {
                     return data.data;
                 });
             }
         };
     })

 .factory('single_list_services', function($http) {
         return {
             getsingleproduct: function() {
                 return $http.get('/json/select_single_product.json').then(function(data) {
                     return data.data;
                 });
             }
         };
     })
     .factory('brand_list_services', function($http) {
         return {
             getBrand: function() {
                 return $http.get('/json/brand_list.json').then(function(data) {
                     return data.data;
                 });
             }
         };
     })

 .factory('notification', function($http) {
     return {
         send: function(data) {
             return $http.post(url_new + 'test_notification', {}).then(function(data) {
                 return data.data;
             });
         }
     };
 })

 .factory('mylaipush', function($http) {
     return {
         notification: function(obj) {
             $http.post('https://push.mylaporetoday.in/device_register', obj).then(function(result) {
                 if (result.status == "success") {} else if (result.status == "error") {
                     console.log(result.message);
                 }
             }, function(error) {
                 if (error == 500 || error == 404 || error == 0) {
                     console.log("Push Api" + error);
                 }
             });
         },
     };
 })

 .factory('Map', function($http, $q) {
     var marker;
     var line;
     var map;
     return {
         init: function(lat, lon) {

             map = new google.maps.Map(document.getElementById('map'), {
                 zoom: 12,
                 center: new google.maps.LatLng(lat, lon)
             });
             places = new google.maps.places.PlacesService(map);
         },
         Marker: function(res) {
             marker = new google.maps.Marker({
                 map: map,
                 position: { lat: res.lat, lng: res.lng },
                 animation: null,
                 title: res.customer_name
             });
         },
         addMarker: function(res, icon) {
             if (line != null) {
                 line.setMap(null);
             }
             if (marker != null) {
                 marker.setMap(null);
             }
             var lineSymbol = {
                 path: google.maps.SymbolPath.CIRCLE,
                 scale: 6,
                 strokeOpacity: 0.8,
                 strokeWeight: 1,
                 fillOpacity: 0.8,
                 fillColor: '#095FDC',
                 strokeColor: '#095FDC',
             };
             marker = new google.maps.Marker({
                 map: map,
                 position: { lat: res.lat, lng: res.lng },
                 icon: lineSymbol,
                 animation: null,
                 title: res.customer_name
             });
         },
         autosearch: function(input) {
             autocomplete = new google.maps.places.Autocomplete(input);
             autocomplete.bindTo('bounds', map);
             return autocomplete;
         },
         search: function(str) {
             var d = $q.defer();
             places.textSearch({ query: str }, function(results, status) {
                 if (status == 'OK') {
                     d.resolve(results[0]);
                 } else d.reject(status);
             });
             return d.promise;
         },
         direction: function(waypts, start, end) {
             var directionsService = new google.maps.DirectionsService;
             var directionsDisplay = new google.maps.DirectionsRenderer;

             directionsDisplay.setMap(map);

             directionsService.route({
                 origin: start,
                 destination: end,
                 waypoints: waypts,
                 optimizeWaypoints: false,
                 travelMode: 'DRIVING'
             }, function(response, status) {
                 if (status === 'OK') {
                     console.log(response);
                     directionsDisplay.setDirections(response);

                 } else {
                     alert('Directions request failed due to ' + status);
                 }
             });
         },
         polyline: function(coords) {
             if (line != null) {
                 line.setMap(null);
             }
             var lineSymbol = {
                 path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
             };
             var line = new google.maps.Polyline({
                 path: coords,
                 fillOpacity: 0.8,
                 fillColor: '#FF0000',
                 scale: 6,
                 strokeColor: '#FF0000',
                 strokeOpacity: 0.8,
                 strokeWeight: 4,
                 icons: [{
                     icon: lineSymbol,
                     offset: '100%'
                 }],
                 map: map
             });
         }
     }
 });
